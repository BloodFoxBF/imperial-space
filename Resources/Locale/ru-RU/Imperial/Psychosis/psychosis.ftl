ent-WeaponAntiqueLaserFake = {ent-WeaponAntiqueLaser}
    .desc = Вся отделка выполнена на никаком уровне. Он украшен кожей иана. Предмет устрашает своей бесполезностью.
    .suffix = Ненастоящий, не спавнить
ent-C4Fake = {ent-C4}
    .desc = Используется для проделывания неаккуратных, никаких, брешей в местах мозга. Любимое средство психологов.
    .suffix = Ненастоящий, не спавнить
ent-PresentFake = {ent-PresentRandomInsane}
    .desc = Несуществующая коробочка с бесполезными сюрпризами внутри.
    .suffix = Ненастоящий, не спавнить
ent-ExGrenadeFake = {ent-ExGrenade}
    .desc = Граната, создающая ничего.
    .suffix = Ненастоящий, не спавнить
ent-WeaponRifleLecterFake = Лектер
    .desc = Второклассная армейская штурмовая винтовка. Использует патроны калибра .70 антиматериальный.
    .suffix = Ненастоящий, не спавнить
ent-ClothingHeadHatCatEarsFake = {ent-ClothingHeadHatCatEars}
    .desc = Ты думал тут будут кошачьи уши?
    .suffix = Ненастоящий, не спавнить
ent-ClothingUniformJumpskirtJanimaidminiFake = {ent-ClothingUniformJumpskirtJanimaidmini}
    .desc = Ты думал тут будет форма горничной?
    .suffix = Ненастоящий, не спавнить
ent-ClothingHandsGlovesColorYellowFake = {ent-ClothingHandsGlovesColorYellow}
    .desc = Эти перчатки защищат вас от пришельцев...?
    .suffix = Ненастоящий, не спавнить
ent-ClothingBeltChiefEngineerFilledFake = {ent-ClothingBeltChiefEngineerFilled}
    .desc = Очень вместительный пояс старшего инженера, жаль бесполезный.
    .suffix = Ненастоящий, не спавнить
ent-NukeDiskFakeFake = {ent-NukeDisk}
    .desc = Диск ядерной авторизации, способный рассмешить ядерную бомбу, при использовании вместе с выпивкой. Записка от Nanotrasen гласит: "ЭТО ВАШЕ ВООБРАЖЕНИЕ".
    .suffix = Ненастоящий, не спавнить
ent-StimpackFake = {ent-Stimpack}
    .desc = Инъектор, содержащий бесконечную дозу кайфа. Используйте когда уверены, что успеете оставаться в здравом уме.
    .suffix = Ненастоящий, не спавнить

ent-MobHumanSyndicateAgentFake = молодого возраста пассажир мужчина
    .desc = Ээээ..
    .suffix = Ненастоящий, не спавнить
ent-MobHumanSpaceNinjaFake = молодого возраста пассажир мужчина
    .desc = Ээээ..
    .suffix = Ненастоящий, не спавнить
ent-MobHumanSyndicateAgentNukeopsFake = молодого возраста пассажир мужчина
    .desc = Ээээ..
    .suffix = Ненастоящий, не спавнить
ent-MobHumanLoneNuclearOperativeFake = молодого возраста пассажир мужчина
    .desc = Ээээ..
    .suffix = Ненастоящий, не спавнить

psychosis-first-stage-heal-effect = Лечит первую стадию психоза, если подходит по листку
psychosis-second-stage-heal-effect = Лечит вторую или меньше стадию психоза, если подходит по листку
psychosis-third-stage-heal-effect = Лечит третью или меньше стадию, если подходит по листку
reagent-name-betterpax = Майндресторер
reagent-desc-betterpax = Восстанавливает психическое разумие пациента, при этом немного разрушая мозг (использовать только при критических стадиях!)


psychosis-heal-psychosis-1 = [head=1]╔═════════════════════════════════╗[/head]
psychosis-heal-psychosis-2 = [color=#19C8D5][head=6]        ░███░██░░░██░████░████░░░░░░░[/color][/head]
psychosis-heal-psychosis-3 = [color=#19C8D5][head=6]        ░██░░███░███░██░░░██░░██░░░░░[/color][/head]
psychosis-heal-psychosis-4 = [color=#19C8D5][head=6]        ░█░░░██░█░██░████░██░░██░░░█░[/color][/head]
psychosis-heal-psychosis-5 = [color=#19C8D5][head=6]        ░░░░░██░░░██░██░░░██░░██░░██░[/color][/head]
psychosis-heal-psychosis-6 = [color=#19C8D5][head=6]        ░░░░░██░░░██░████░████░░░███░[/color][/head]
psychosis-heal-psychosis-7 = [head=1]╚═════════════════════════════════╝[/head]
psychosis-heal-psychosis-8 = [head=2]╔══════════════════════════╗[/head]
psychosis-heal-psychosis-9 = [head=2]   ХАРАКТЕРИСТИКА ПСИХОЗА [/head]
psychosis-heal-psychosis-10 = [head=2]╚══════════════════════════╝[/head]
psychosis-heal-psychosis-11 = ╓———————————————————————————╖
psychosis-heal-psychosis-12 = • [head=5][color=#5F9EA0]Первая стадия:[/color][/head]
psychosis-heal-psychosis-13 = • Актуальный препарат для противодействия: {$first}
psychosis-heal-psychosis-14 = • Симптомы стадии:[color=#4B57D5] Звуковые галюцинации
psychosis-heal-psychosis-15 = [/color]
psychosis-heal-psychosis-16 = • [head=5][color=#5F9EA0]Вторая стадия:[/color][/head]
psychosis-heal-psychosis-17 = • Актуальный препарат для противодействия: {$second}
psychosis-heal-psychosis-18 = • Симптомы стадии:[color=#4B57D5] Звуковые, а так же материальные галюцинации, иногда невнятная речь.
psychosis-heal-psychosis-19 = [/color]
psychosis-heal-psychosis-20 = • [head=5][color=#5F9EA0]Третья стадия:[/color][/head]
psychosis-heal-psychosis-21 = • Актуальный препарат для противодействия: {$third}
psychosis-heal-psychosis-22 = • Симптомы стадии:[color=#4B57D5] Звуковые, а так же материальные галюцинации, иногда невнятная речь, а так же оторванность от реальности.
psychosis-heal-psychosis-23 = [/color]
psychosis-heal-psychosis-24 = ╙———————————————————————————╜
psychosis-heal-psychosis-25 = [empty]
psychosis-heal-psychosis-26 = [italic]ПРИМЕЧАНИЕ: При возникновении вышеперечисленных симптомов пациент должен [bold]немедленно[/bold] обратиться в медицинский блок станции.[/italic]
psychosis-heal-psychosis-27 = [bold]   ————————<Место для печатей>————————[/bold]
psychosis-heal-psychosis-28 = ▶                                                                                                            ◀


psychosis-Nicotine = Никотин
psychosis-Etanol = Этанол
psychosis-Coffee = Кофе


psychosis-THK = ТГК
psychosis-Eth = Этилоксиэфедрин


psychosis-BetterPax = Майндресторер
psychosis-Artif = Артифексиум

ent-PsychosisHealPaper = Текущие способы лечения психоза
    .desc = {ent-Paper.desc}

psychosis-event-announcement = Обнаружена психическая аномалия, персоналу с галюцинациями срочно обратиться в медицинский блок станции.
